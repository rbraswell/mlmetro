#!/usr/bin/env python

from math import log, sqrt, cos
#from numpy.math cimport INFINITY

from numpy.random import random as rand
from numpy import pi
import numpy as np

INF = 1.0e200

def randn():
    # use the Box-Muller transform
    u1 = rand()
    u2 = rand()
    return sqrt(-2.0*log(u1))*cos(2.0*pi*u2)

def linmodel(param,input):
    n = input.shape[0]
    output = np.zeros_like(input)
    for i in range(n):
        output[i] = param[0] + param[1]*input[i]
    return output

def metroCost(param, data, model, input, prior, type, errweights=None):

    n = data.shape[0]
    nparam = param.shape[0]

    print prior.shape
    print prior
    
    if type == 'u':
        for iparam in range(nparam):
            if param[iparam] < prior[iparam,0] or param[iparam] > prior[iparam,1]:
                reg = INF
    else:
        reg = 0.0
        for iparam in range(nparam):
            reg = reg + ((param[iparam] - prior[iparam,0])**2 / (2.0*prior[iparam,1]**2))

    estimate = model(param, input)

    ssqerr = 0.0
    for i in range(n):
        diff = (estimate[i] - data[i])
        ssqerr = ssqerr + errweights[i]*diff*diff
    sigma = abs(sqrt(ssqerr/(n + sqrt(2.0*n)*randn())))
    J = n*log(sigma) + ssqerr/(2*sigma**2) + reg

    return J, sigma




n = 100

input = np.random.random(n)

param = np.array([0.0, 1.0])

output = linmodel(param, input)
data1 = output + 0.1*np.random.random(n)
data2 = output + 0.0001*np.random.random(n)

print output

errweights = np.ones(n)

type = 'n'

model = linmodel

prior = np.zeros((2,2))
prior[0,0] = -0.5
prior[0,1] = 0.5
prior[1,0] = 0.5
prior[1,1] = 1.5
prior[:,1] = 1.0


guess = np.array([0.5, 1.5])

J, sigma = metroCost(param, data1, model, input, prior, type, errweights)
print J
print sigma

J, sigma = metroCost(param, data2, model, input, prior, type, errweights)
print J
print sigma

J, sigma = metroCost(guess, data1, model, input, prior, type, errweights)
print J
print sigma

J, sigma = metroCost(guess, data2, model, input, prior, type, errweights)
print J
print sigma

