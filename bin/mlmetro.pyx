import numpy as np
cimport numpy as np
cimport cython

from libc.math cimport log, sqrt, cos, exp, M_PI
from libc.stdlib cimport rand, RAND_MAX

from numpy.math cimport INFINITY as INF

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)


def randu():
    # random uniform number
    return <float>rand()/<float>RAND_MAX

def randn():
    # use the Box-Muller transform
    cdef float u1 = randu()
    cdef float u2 = randu()
    return sqrt(-2.0*log(u1))*cos(2.0*M_PI*u2)

def linmodel(np.ndarray[np.float32_t, ndim=1, negative_indices=False] param,
             np.ndarray[np.float32_t, ndim=1, negative_indices=False] inputs):
    cdef int n = inputs.shape[0]
    cdef np.ndarray[np.float32_t, ndim=1, negative_indices=False]\
        output = np.zeros(n, dtype='float32')
    cdef int i
    for i in range(n):
        output[i] = param[0] + param[1]*inputs[i]
    return output

def metroCost(param,
              data,
              model,
              inputs,
              prior,
              btype,
              errweights=None):
    cdef int n = data.shape[0]
    nparam = param.shape[0]
    if errweights is None:
        errweights = np.ones(n)
    reg = 0.0
    if btype == 'u':
        for iparam in range(nparam):
            if param[iparam] < prior[0,iparam] or param[iparam] > prior[1,iparam]:
                reg = INF
    else:
        for iparam in range(nparam):
            reg = reg + (param[iparam] - prior[0,iparam])**2 / 2.0*prior[1,iparam]**2
    estimate = model(param, inputs)
    ssqerr = 0.0
    for i in range(n):
        diff = (estimate[i] - data[i])
        ssqerr = ssqerr + errweights[i]*diff*diff
    sigma = abs(sqrt(ssqerr/(n + sqrt(2.0*n)*randn())))
    J = n*log(sigma) + ssqerr/(2*sigma**2) + reg
    return J, sigma


def metro(data, model, prior, btype, inputs, nkeep):
    assert btype in ('u', 'n')

    target = 0.4   # target acceptance rate
    nrate = 500    # size of pool for acceprance rate
    thresh = 0.005 # tolerance for acceptance rate
    itemp = 0.3    # initial temperature
    cool = 0.99    # temperature cooling factor
    wait = 2000    # additional burn in after convergence
    
    # get first guess parameter
    if btype == 'u':
        lb = prior[0,:]
        ub = prior[1,:]
        param = lb + 0.5*(ub - lb)
    else:
        param = prior[0,:]
        lb = prior[0,:] - 2.0*prior[1,:]
        ub = prior[0,:] + 2.0*prior[1,:]

    print param
    nparam = len(param)

    freeparam = np.where(lb != ub)[0]
    nfreeparam = len(freeparam)

    # initial temperature for each parameter
    temp = itemp*np.ones(nparam)
        
    # initialize cost
    cost, sigma = metroCost(param, data, model, inputs, prior, btype)

    # calculate temperature increase factor
    warm = cool**((target-1.0)/target)

    done = 0
    converged = 0
    i = 0
    k = 0
    sumaccept = 0.0
    recentaccept = np.zeros(nrate)

    X = np.ones((nkeep,nparam))
    J = np.ones(nkeep)
    R = np.ones(nkeep)
    A = np.ones(nkeep)
    T = np.ones((nkeep,nparam))
    S = np.ones(nkeep)
    
    while k < wait + nkeep:

        # proposal step
        iparam = freeparam[int(nfreeparam*randu())]
        r = (randu() - 0.5)*temp[iparam]
        paramnew = param.copy()
        paramnew[iparam] = paramnew[iparam] + r*(ub[iparam] - lb[iparam])

        # cost at proposed step
        costnew, sigmanew = metroCost(paramnew,data,model,inputs,prior,btype)

        # possibly accept
        acceptprob = min([1.0, exp(cost - costnew)])
        accept = (randu() < acceptprob)

        print i, k, cost, costnew, acceptprob, accept, paramnew
        
        if accept:
             param = paramnew.copy()
             cost = costnew
             sigma = sigmanew

        # update acceptance rates
        sumaccept = sumaccept + accept
        cumaccept = sumaccept/float(i + 1)
        recentaccept[:-1] = recentaccept[1:]
        recentaccept[-1] = accept

        if i < nrate:
            instaccept = cumaccept
        else:
            instaccept = recentaccept.mean()

        if accept:
            temp[iparam] = temp[iparam]*warm
        else:
            temp[iparam] = temp[iparam]*cool

        if converged == 0:
            # check for convergence
            if i > nrate and abs(instaccept - target) < thresh:
                converged = 1
        elif converged == 1:
            # accumulate parameter steps after additional burn in
            if k >= wait:
                ikeep = k - wait
                X[ikeep,:] = param
                J[ikeep] = cost
                R[ikeep] = instaccept
                A[ikeep] = cumaccept
                T[ikeep,:] = temp
                S[ikeep] = sigma
            k = k + 1
        i = i + 1

    return X, J, R, A, T, S



#aveu = 0.0
#aven = 0.0
#for i in range(10000):
#    aveu = aveu + randu()
#    aven = aven + randn()
#print aveu/1000.0
#print aven/1000.0

    

n = 100
inputs = np.random.random(n)

param = np.array([0.0, 1.0])

output = linmodel(param, inputs)
data = output + 0.01*np.random.random(n)

print output

errweights = np.ones(n)

btype = 'n'

model = linmodel

prior = np.zeros((2,2))

btype = 'n'
prior[0,0] = -0.1
prior[0,1] = 1.3
prior[1,0] = 0.9
prior[1,1] = 0.9

#btype = 'u'
#prior[0,0] = -0.1
#prior[0,1] = 0.6
#prior[1,0] = 0.4
#prior[1,1] = 1.6


numiter = 1000
model = linmodel

X, J, R, A, T, S = metro(data, model, prior, btype, inputs, numiter)

print X.mean(axis=0)
print X.std(axis=0)

