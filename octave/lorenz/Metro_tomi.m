function [X,J,R,A,T,S,ikeep]=Metro(data,model,prior,type,input,numiter,options)
% function [X,J,R,A,T,S]=Metro(data,model,prior,type,input,numiter,options)
% MCMC method for model parameter estimation
% Prior type is 'u' (bounded uniform) or 'n' (normal)
% 	if 'u' then prior is an array [low high]
% 	if 'n' then prior is an array [mean std]
% Additional parameters can get passed through to the model using "input"
% The model should be of the form Model(param) or Model(param,input)
% "param" has to be an array (1xN) but input can be anything


if (nargin<4 | isempty(type)) type='u';end
if (nargin<5) input=[];end
if (nargin<6 | isempty(numiter)) numiter=1000;end

warning off;

% Get parameter starting values
if (type=='u') 
    lb=prior(1,:);
    ub=prior(2,:);
    param=lb+0.5*(ub-lb);
    pguess=param
else
    param=prior(1,:);
    lb=prior(1,:)-2*prior(2,:);
    ub=prior(1,:)+2*prior(2,:);
end

nparam=length(param);

% Locate fixed paramters if any
fixparam=find(lb==ub);
freeparam=find(lb~=ub);
nfixparam=length(fixparam);
nfreeparam=length(freeparam);

% Include default options if not specified
if (nargin<7) 
    options=metroOptions;
end    

% Set options
target=options.target;
itemp=options.itemp;
nrate=options.nrate;
cool=options.cool;
thresh=options.thresh;
disp=options.disp;
keep=options.keep;
tgroup=options.tgroup;
wait=options.wait;

% Initialize temperatures and counters
done=0; converged=0; i=0; k=0;
temp=itemp*ones(1,nparam);
if (disp==0) disp=10^9;end

% Initialize cost
[cost sigma]=metroCost(param,data,model,input,prior,type);

% Calculate temperature increase factor
warm=cool^((target-1)/target);

% Main loop
while (~done)
    i=i+1;
    iparam=freeparam(1+fix(nfreeparam*rand(1,1)));
    r= (rand-0.5)*temp(iparam);
    paramnew=param;
    paramnew(iparam)=paramnew(iparam)+r*(ub(iparam)-lb(iparam));
    [costnew sigmanew]=metroCost(paramnew,data,model,input,prior,type);
    acceptprob=min(1,exp(cost-costnew));
    accept(i)=(rand<acceptprob);
    if (accept(i))          
        param=paramnew;
        cost=costnew;
        sigma=sigmanew;
    end
    cumaccept=sum(accept)/i;
    if (i>nrate) instaccept=mean(accept(i-nrate+1:i));else instaccept=cumaccept;end    

    if (converged==2)
        k=k+1;
        if (~mod(k,disp)) fprintf(1,'== %d %d %6f %6f %6f %d %6f %6f\n',i,k,cumaccept,instaccept,cost,iparam,temp(iparam),paramnew(iparam));end
        if (k==numiter) done=1;end
    elseif (converged==1)
        k=k+1;
        if (accept(i)) temp(iparam)=temp(iparam)*warm;else temp(iparam)=temp(iparam)*cool;end        
        if (~mod(k,disp)) fprintf(1,'++ %d %d %6f %6f %6f %d %6f %6f\n',i,k,cumaccept,instaccept,cost,iparam,temp(iparam),paramnew(iparam));end
        if (k==wait)
            k=0;
            converged=2;
            ikeep=i;                
	    %tgroup
	    %k
	    %min(tgroup,i)
            %size(T)
            %i
            %for m=1:nparam temp(m)=mean(T(i-min(tgroup,i+1):i-1,m));end
            temp=mean(T(i-tgroup:i-1,:));
            %fprintf(1,'   READY AT %d\n',i);
            %fprintf(1,'   temp = %f\n',temp);
        end
    else        
        if (accept(i)) temp(iparam)=temp(iparam)*warm;else temp(iparam)=temp(iparam)*cool;end
        if (~mod(i,disp)) fprintf(1,'-- %d %d %6f %6f %6f %d %6f %6f\n',i,k,cumaccept,instaccept,cost,iparam,temp(iparam),paramnew(iparam));end
        if (i > nrate & abs(instaccept - target) < thresh) 
            converged=1;
            %fprintf(1,'   CONVERGED AT %d\n',i);
            icon=i;            
        end
    end

    X(i,:)=param;
    J(i)=cost;
    A(i)=cumaccept;
    T(i,:)=temp;
    S(i)=sigma;
    R(i)=instaccept;
end
    
if (keep==0)
    X=X(ikeep:end,:);
    J=J(ikeep:end);
    A=A(ikeep:end);
    T=T(ikeep:end,:);
    S=S(ikeep:end);
    R=R(ikeep:end);
    ikeep=1;
end    

warning on;
