function options=metroOptions(varargin)

defaultoptions=struct(...
    'target',0.40,...
    'itemp',0.3,...
    'cool',0.990,...
    'thresh',0.01,...
    'disp',250,...
    'nrate',500,...
    'keep',0,...
    'tgroup',1000,...
    'wait',2000 ...
);

if ((nargin==0)&(nargout==0))
    fprintf(['   target: [positive scalar | {' num2str(defaultoptions.target) '}]\n']);
    fprintf(['    itemp: [positive scalar | {' num2str(defaultoptions.itemp) '}]\n']);
    fprintf(['    nrate: [positive scalar | {' num2str(defaultoptions.nrate) '}]\n']);
    fprintf(['     cool: [positive scalar | {' num2str(defaultoptions.cool) '}]\n']);
    fprintf(['   thresh: [positive scalar | {' num2str(defaultoptions.thresh) '}]\n']);
    fprintf(['     disp: [positive scalar | {' num2str(defaultoptions.disp) '}]\n']);
    fprintf(['     keep: [positive scalar | {' num2str(defaultoptions.keep) '}]\n']);
    fprintf(['   tgroup: [positive scalar | {' num2str(defaultoptions.tgroup) '}]\n']);
    fprintf(['     wait: [positive scalar | {' num2str(defaultoptions.wait) '}]\n']);
    fprintf('\n');
elseif ((nargout==1)&(nargin==0))
    options=defaultoptions;
else
    if (mod(nargin,2))
        options=varargin{1}
        for k=1:fix(nargin/2)    
            eval(['options.' varargin{2*k} ' = ' num2str(varargin{2*k+1}) ';']);
        end
    else
        options=defaultoptions;
        for k=1:fix(nargin/2)    
            eval(['options.' varargin{2*k-1} ' = ' num2str(varargin{2*k}) ';']);
        end
    end    
end






