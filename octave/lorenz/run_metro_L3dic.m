% This is a test of the MC-Metro algorithm applied to Lorenz 3-component model
% to estimate the model initial condition

% GENERATE  MODEL TRUTH 

%---------------------------------------------------------------------
%   1. Set parameter values
%---------------------------------------------------------------------
%
format long
%coefficients
a=1.0d1;       % sigma
b=8.0d0/3.0d0; % rho
r=2.8d1;       % beta
%

%---------------------------------------------------------------------
%  2. Set initial conditions
%---------------------------------------------------------------------
truex=1.;
truey=1.;
truez=1.;

%---------------------------------------------------------------------
%  3. Set integration time parameters
% integration  window
ta=2;
%timestep
h=0.05;

% total number of time steps
fcstep = ta/h+1;

guessx(1)=truex;
guessy(1)=truey;
guessz(1)=truez;
j=0;

params=[guessx guessy guessz];
inputs=[fcstep h j a r b];

% Run model
%true state [xx yy zz]
[xx,yy,zz]=Lorenz3dic(params,inputs);


% GENERATE ARTIFICIAL OBSERVATRIONS by adding random noise N(0,1)

     xp=0.2*mean(xx);
     yp=0.1*mean(yy);
     zp=0.1*mean(zz);
     

noise=xp*randn(size(xx));
xxdata=xx+noise;
noise=yp*randn(size(yy));
yydata=yy+noise;
noise=zp*randn(size(zz));
zzdata=zz+noise;

% X component is used for observations
xdat=xxdata;
xtrue=xx;

% SET PRIORS - BOUNDED UNIFORM
 prior=[0 0 0; 4 4 4];

% Run Metropolis
[pnew,cost,instacc,cumacc,temp,sigma]=Metro(xdat,'Lorenz3dic',prior,'u',inputs,40000,metroOptions);


pbest=mean(pnew);
[xn yn zn]=Lorenz3dic(pbest,inputs);


% Ploting .........................


%plot(time,xx,time,xn,time,xstate_inc);
%legend('truth','best','best-truth',0);
%xlabel('time');ylabel('X');
%print -djpeg modeldata.jpg


%xbin_inc=(10.2-9.5)/30;
%xbin=9.5:xbin_inc:10.2;
%ybin_inc=(28.6-27.6)/30;
%ybin=27.6:ybin_inc:28.6;
%zbin_inc=(2.74-2.62)/30;
%zbin=2.62:zbin_inc:2.74;

%hist(pnew(:,1),xbin);
%hist(pnew(:,1),30);
%xlabel('X');ylabel('Count');
%print -djpeg a_hist.jpg;

%hist(pnew(:,1),30)
%xlabel('Y');ylabel('Count');
%print -djpeg X_hist;
