function [J,sigma]=metroCost(param,data,model,input,prior,type,errweights,opttype)
% function [J,sigma]=metroCost(param,data,model,input,prior,type,errweights,opttype)
% 

sz=size(data);
n=sz(1);
ndim=sz(2);

%if (nargin<7 | isempty(errweights)) errweights=ones(n,ndim);end
%if (nargin<8 | isempty(opttype)) opttype='n';end
if (nargin<7) errweights=ones(n,ndim);end
if (nargin<8) opttype='n';end

    
if (type=='u')
    reg=-log(prod(fix((param>=prior(1,:) & param<=prior(2,:)))));
    if (isinf(reg))
        J=Inf;
        sigma=Inf;
        return;
    else
        if (nargin<4)
	        estimate=feval(model,param);
        else
	        estimate=feval(model,param,input);
        end    
        ssqerr=sum(errweights.*((estimate-data).^2));
        if (nargin<7) 
            sigma=abs(sqrt(ssqerr/(n+sqrt(2*n)*randn)));
        end
        J = n*log(sigma) + ssqerr/(2*sigma^2) + reg;
    end        
else
    reg=sum(((param-prior(1,:)).^2)./(2*prior(2,:).^2));
    if (nargin<4)
	    estimate=feval(model,param);
    else
	    estimate=feval(model,param,input);
    end    
    ssqerr=sum(errweights.*((estimate-data).^2));
    if (nargin<7) 
        sigma=abs(sqrt(ssqerr/(n+sqrt(2*n)*randn)));
    end
    J = n*log(sigma) + ssqerr/(2*sigma^2) + reg;
end

%if (isnan(J)) fprintf(1,' NAN  %f %f %f %f %f %f\n',n,sigma,ssqerr,reg,length(find(isinf(estimate))),length(find(isinf(data))));end
%if (isinf(J)) fprintf(1,' INF  %f %f %f %f %f %f\n',n,sigma,ssqerr,reg,length(find(isnan(estimate))),length(find(isnan(data))));end

