function [X,J,R,A,T,S]=Metro(data,model,prior,type,input,numiter,options)
% function [X,J,R,A,T,S]=Metro(data,model,prior,type,input,numiter,options)
% MCMC method for model parameter estimation
% Prior type is 'u' (bounded uniform) or 'n' (normal)
% 	if 'u' then prior is an array [low high]
% 	if 'n' then prior is an array [mean std]
% Additional parameters can get passed through to the model using "input"

if (nargin<4 | isempty(type)) type='u';end
if (nargin<5) input=[];end
if (nargin<6 | isempty(numiter)) numiter=1000;end

warning off;

% Get parameter starting values
if (type=='u') 
    lb=prior(1,:);
    ub=prior(2,:);
    param=lb+0.5*(ub-lb);
else
    param=prior(1,:);
    lb=prior(1,:)-2*prior(2,:);
    ub=prior(1,:)+2*prior(2,:);
end

nparam=length(param);

% Locate fixed paramters if any
fixparam=find(lb==ub);
freeparam=find(lb~=ub);
nfixparam=length(fixparam);
nfreeparam=length(freeparam);

% Include default options if not specified
if (nargin<7) 
    options=metroOptions;
end    

% Set options
cool=options.cool;
thresh=options.thresh;
disp=options.disp;
rate=options.rate;
nrate=options.nrate;

% Initialize temperature and counters
temp=1.0; done=0; converged=0; i=0; k=0; lastsgn=0;

% Initialize cost
[cost sigma]=metroCost(param,data,model,input,prior,type);

% Main loop
while (~done)
    i=i+1;
    iparam=freeparam(1+fix(nfreeparam*rand(1,1)));
    r= -0.5*temp+rand(1,1)*temp;
    paramnew=param;
    paramnew(iparam)=paramnew(iparam)+r*(ub(iparam)-lb(iparam));
    [costnew sigmanew]=metroCost(paramnew,data,model,input,prior,type);
    acceptprob=min(1,exp(cost-costnew));
    accept(i)=(rand<acceptprob);
    % UPDATE NEW STATE
    if (accept(i))          
        param=paramnew;
        cost=costnew;
        sigma=sigmanew;
    end    
    cumaccept=sum(accept)/i;
    if (i>nrate) instaccept=mean(accept(i-nrate+1:i));else instaccept=cumaccept;end
    if (converged)
        k=k+1;
        if (k==1) fprintf(1,'** Converged\n');end
        if (~mod(k,disp)) fprintf(1,'++ %d %d %f %f %f %f\n',i,k,cumaccept,instaccept,cost,temp);end
        if (k==numiter) done=1;end
    elseif (~mod(i,nrate))       % (i>nrate)
        % CONVERGENCE CRITERIA
        sgn=sign(instaccept-rate);
        if (sgn*lastsgn== -1) cool=cool/2;end
        lastsgn=sgn;
        if (i>nrate & abs(cumaccept-rate) < thresh) 
            converged=1;
        elseif (i>nrate)
            if (instaccept>rate)
                temp=temp+cool;
            else 
                temp=temp-cool;
            end
        end
    end    
    if (~mod(i,disp)) fprintf(1,'-- %d %d %f %f %f %f %f\n',i,k,cumaccept,instaccept,cost,temp,cool);end
    
    X(i,:)=param;
    J(i)=cost;
    A(i)=cumaccept;
    T(i)=temp;
    S(i)=sigma;
    R(i)=instaccept;

end

warning on;
