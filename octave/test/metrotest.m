% This is just a simple test of the ML-Metro algorithm

% YOU DO NOT NEED THE FOLLOWING LINE IF YOU RUN IT IN THE SAME DIRECTORY AS THE METRO CODE
addpath(path, '/home/rbraswell/repo/mlmetro/octave');

% GENERATE A TEST DATA SET
params=[2 2];
n=25;
m=50;
time=100*rand(n+m,1);
time=time(randomsubset(n+m,n/(n+m),123,1));

linmodel=@(p,t) p(1)+p(2)*t;

xtrue=linmodel(params,time);
noise=50.*randn(n,1);
xdat=xtrue+noise;
ts=linspace(min(time),max(time),100);
pfit=polyfit(time,xdat,1);
xfit=polyval(pfit,ts);

% SET PRIORS - BOUNDED UNIFORM
prior=[0 0; 10 10];

% THE ONLY IMPORTANT MLMETRO LINE OF CODE IN THIS EXAMPLE
[x,c,r,a,t,s]=metro(xdat,linmodel,prior,'u',time,20000,metroOptions);

% USE THE MEAN OF THE POSTERIOR TO SHOW RESULT
pbest=mean(x);
xbest=pbest(1)+pbest(2)*ts;
sbest=mean(s);

fprintf(1, 'DONE\n')

% MAKE SOME FIGURES

%x = -10:0.1:10;
%plot (x, sin(x));


%plot(time,xdat,'o',time,xtrue,'-',ts,xbest,'-',ts,xfit,'-');
%legend({'Data','True','Metropolis','Regression'});
%print -djpeg modeldatafit.jpg

%plot(time,xdat,'o',ts,xbest,'k',ts,xbest-sbest,'k--',ts,xbest+sbest,'k--');
%legend('Data','Metropolis','1-Sigma Range',0);
%print -djpeg modeldatarange.jpg

%hist(x(:,1),30)
%xlabel('Y-Intercept');ylabel('Count');
%print -djpeg yinthist.jpg

%hist(x(:,2),30)
%xlabel('Slope');ylabel('Count');
%print -djpeg slopehist.jpg

%hist(s,30);
%xlabel('Sigma');
%ylabel('Count');
%print -djpeg sigmahist.jpg

%plot(x);
%xlabel('Iteration');
%ylabel('Parameter value');
%legend('Y-Intercept','Slope');
%print -djpeg xtrace.jpg
