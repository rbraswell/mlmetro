function [isub, irem]=randomsubset(Q,f,randstate,dosort)
% function [isub, irem]=randomsubset(Q,f,randstate,dosort)
%
% This provides a list of f*Q mutually exclusive random
% indices in the range 1 to Q. The remaining (1-f)*Q
% values can also be returned. Both vectors are sorted
% from low to high.
%
% Use randstate to specify a seed for repeatable sub-
% sampling. Otherwise the results will be different each
% time.

if (nargin < 4) dosort=1;end
if (nargin < 3 | isempty(randstate)) randstate=sum(100*clock);end
if (nargin < 2) error('Specify Q and f');end
if (f > 1 | f< 0) error('Check subset fraction');end

nSub=fix(f*Q);
qRnd = 1:Q;
rand('state',randstate);

for i=1:Q j=1+fix(Q*rand(1,1));qRnd([i j])=qRnd([j i]);end;

if (dosort)
	isub=sort(qRnd(1:nSub));
	irem=sort(qRnd(nSub+1:end));
else
	isub=qRnd(1:nSub);
	irem=qRnd(nSub+1:end);
end
